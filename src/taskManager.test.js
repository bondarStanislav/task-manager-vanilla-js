import { expect } from 'chai';
import 'mock-local-storage';
import {
  addTask,
  deleteTask,
  clearCompletedTasks,
  toggleTaskCompletion,
  filterTasks,
  saveTasksToLocalStorage,
} from './index.ts';

describe('TaskManager', () => {
  let tasks = [];

  beforeEach(() => {
    // Clean up tasks before each test
    tasks = [];
    localStorage.clear();
  });

  it('addTask adds a task', () => {
    addTask('New Task');
    expect(tasks).to.have.lengthOf(1);
  });

  it('deleteTask deletes a task', () => {
    const taskId = '1';
    tasks.push({ id: taskId, content: 'Task', completed: false });
    deleteTask(taskId);
    expect(tasks).to.have.lengthOf(0);
  });

  it('clearCompletedTasks clears completed tasks', () => {
    tasks.push({ id: '1', content: 'Task 1', completed: true });
    tasks.push({ id: '2', content: 'Task 2', completed: false });
    clearCompletedTasks();
    expect(tasks).to.have.lengthOf(1);
  });

  it('toggleTaskCompletion toggles task completion', () => {
    const taskId = '1';
    tasks.push({ id: taskId, content: 'Task', completed: false });
    toggleTaskCompletion(taskId);
    expect(tasks[0].completed).to.be.true;
  });

  it('filterTasks filters tasks based on completion', () => {
    tasks.push({ id: '1', content: 'Task 1', completed: true });
    tasks.push({ id: '2', content: 'Task 2', completed: false });
    const activeTasks = filterTasks(Filter.Active);
    expect(activeTasks).to.have.lengthOf(1);
  });

  it('saveTasksToLocalStorage saves tasks to localStorage', () => {
    tasks.push({ id: '1', content: 'Task', completed: false });
    saveTasksToLocalStorage();
    expect(localStorage.getItem('tasksData')).to.be.equal(
      JSON.stringify(tasks),
    );
  });
});

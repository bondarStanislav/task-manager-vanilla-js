// util.ts
export function isValidTask(task: string) {
  return task.trim().length > 0;
}

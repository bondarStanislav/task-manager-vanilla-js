import './styles.scss';

// Declaring types for App
export interface Task {
  id: string;
  content: string;
  completed: boolean;
}

export enum Filter {
  All,
  Active,
  Completed,
}

// Initialize tasks and filter
let tasks: Task[] = JSON.parse(localStorage.getItem('tasksData') || '[]');
let currentFilter: Filter = Filter.All;

// Get all needed elements in DOM
const elements = {
  app: document.getElementById('app') as HTMLDivElement,
  taskInput: document.getElementById('todo-input') as HTMLInputElement,
  taskList: document.getElementById('todo-list') as HTMLUListElement,
  deleteCompletedButton: document.getElementById(
    'clear-completed',
  ) as HTMLButtonElement,
  taskCounter: document.getElementById('counter') as HTMLDivElement,
  filterContainer: document.getElementById(
    'filter-container',
  ) as HTMLButtonElement,
  filterButtons: {
    all: document.getElementById('show-all') as HTMLButtonElement,
    active: document.getElementById('show-active') as HTMLButtonElement,
    completed: document.getElementById('show-completed') as HTMLButtonElement,
  },
};

// Action handlers
export function addTask(content: string): void {
  tasks.push({ id: Date.now().toString(), content, completed: false });
  updateApp();
}

export function deleteTask(id: string): void {
  tasks = tasks.filter((task) => task.id !== id);
  updateApp();
}

export function clearCompletedTasks(): void {
  tasks = tasks.filter((task) => !task.completed);
  updateApp();
}

export function toggleTaskCompletion(id: string): void {
  tasks = tasks.map((task) =>
    task.id === id ? { ...task, completed: !task.completed } : task,
  );
  updateApp();
}

// Wrapper to update Local Storage and UI
function updateApp(): void {
  saveTasksToLocalStorage();
  renderTasks();
}

export function saveTasksToLocalStorage(): void {
  localStorage.setItem('tasksData', JSON.stringify(tasks));
}

// Filter template
export function filterTasks(filter: Filter): Task[] {
  switch (filter) {
    case Filter.Active:
      return tasks.filter((task) => !task.completed);
    case Filter.Completed:
      return tasks.filter((task) => task.completed);
    default:
      return tasks;
  }
}

// Filter usage with post-rerender
function setFilter(filter: Filter) {
  currentFilter = filter;
  updateApp();
}

// Event Listeners for filters
elements.filterButtons.all.addEventListener('click', () =>
    setFilter(Filter.All),
);
elements.filterButtons.active.addEventListener('click', () =>
    setFilter(Filter.Active),
);
elements.filterButtons.completed.addEventListener('click', () =>
    setFilter(Filter.Completed),
);
elements.deleteCompletedButton.addEventListener('click', clearCompletedTasks);

// Event Listener to add the task
elements.taskInput.addEventListener('keydown', (event) => {
  if (event.key === 'Enter') {
    const taskContent = elements.taskInput.value.trim();
    if (taskContent !== '') {
      addTask(taskContent);
      elements.taskInput.value = '';
    }
  }
});

// Render tasks received from Local Storage
export function renderTasks(): void {
  // At the beginning we filter tasks with 'All' filter as default
  const filteredTasks = filterTasks(currentFilter);
  elements.taskList.innerHTML = '';

  // Reverse the tasks to render properly
  const reversedTasks = [...filteredTasks].reverse();

  reversedTasks.forEach((task) => {
    // Task item elements
    const listItem = document.createElement('div');
    const checkbox = document.createElement('input');
    const textNode = document.createTextNode(task.content);
    const deleteButton = document.createElement('button');

    // Start checkbox block
    checkbox.type = 'checkbox';
    checkbox.checked = task.completed;
    checkbox.addEventListener('change', () => {
      toggleTaskCompletion(task.id);
    });

    // Add a class when checkbox is checked
    if (checkbox.checked) {
      listItem.classList.add('checkedStyle');
    } else {
      listItem.classList.remove('checkedStyle');
    }
    // End checkbox block

    // Delete block
    deleteButton.className = 'delete-button';
    deleteButton.addEventListener('click', () => deleteTask(task.id));

    // Compose internal elements and render the task
    listItem.append(checkbox, textNode, deleteButton);
    elements.taskList.append(listItem);
  });

  // Check tasks count to display filters or not
  const itemsCount = tasks.length;
  elements.filterContainer.style.display = itemsCount ? 'flex' : 'none';
  if (tasks.length) {
    elements.app.classList.remove('empty');
  } else {
    elements.app.classList.add('empty');
  }

  // Start Uncompleted tasks counter block
  const unCompletedTasksCount = tasks.filter((task) => !task.completed).length;
  switch (unCompletedTasksCount) {
    case 0:
      elements.taskCounter.textContent = `No items left`;
      break;
    case 1:
      elements.taskCounter.textContent = `1 item left`;
      break;
    default:
      elements.taskCounter.textContent = `${unCompletedTasksCount} items left`;
      break;
  }

  const hasCompletedTasks = tasks.some((task) => task.completed);
  elements.deleteCompletedButton.style.visibility = hasCompletedTasks
    ? 'visible'
    : 'hidden';
  // End Uncompleted tasks counter block

  // Filters styling depending on active tab
  Object.values(elements.filterButtons).forEach((button) =>
    button.classList.remove('active'),
  );
  if (currentFilter === Filter.All)
    elements.filterButtons.all.classList.add('active');
  if (currentFilter === Filter.Active)
    elements.filterButtons.active.classList.add('active');
  if (currentFilter === Filter.Completed)
    elements.filterButtons.completed.classList.add('active');
}

//Initial render
renderTasks();

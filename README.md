# Hello, world;) I'm [Stan](https://t.me/st_n_sl_v/)
### this is the Vanilla Js task manager you can use on everyday purpose

It is so lightweight so you don't even need to build by your own, just:

- download the 'dist' folder files
- run index.html

If you're a developer and want to extend, contribute or give me a feedback you need to:
```bash
git clone https://gitlab.com/bondarStanislav/task-manager-vanilla-js.git
```
```bash
cd task-manager-vanilla-js.git
```
```bash
npm i
```
```bash
npm run dev
```